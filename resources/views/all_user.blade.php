<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>all users Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
<div class="container">
    <br />
    @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
    @endif
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Date</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th colspan="2">Action</th>
        </tr>
        </thead>
        <tbody>

        @foreach($userInfos as $user)
            @php
                //$date=date_formate($user['date'],'Y-m-d');
            @endphp
            <tr>
                <td>{{$user['id']}}</td>
                <td>{{$user['full_name']}}</td>
                <td>{{$user->date}}</td>
                <td>{{$user['email']}}</td>
                <td>{{ $user->number }}</td>

                <td><a href="{{url('/user-edit', $user['id'])}}" class="btn btn-warning">Edit</a></td>
                <td><a href="{{url('/user-destroy', $user['id'])}}" class="btn btn-warning">Delete</a></td>
                {{--<td>--}}
                    {{--<form action="{{url('user-destroy', $user['id'])}}" method="post">--}}
                        {{--@csrf--}}
                        {{--<input name="_method" type="hidden" value="DELETE">--}}
                        {{--<button class="btn btn-danger" type="submit">Delete</button>--}}
                    {{--</form>--}}
                {{--</td>--}}
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>