<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/home','WelcomeController@home');

Route::get('/all_users','WelcomeController@all_users');

Route::get('/register','RegistrationController@register');

Route::post('/register','RegistrationController@store');

Route::get('/user-edit/{id}','WelcomeController@editUser');

Route::post('/user-update','WelcomeController@updateUser');

Route::get('/user-destroy/{id}','WelcomeController@deleteUser');
