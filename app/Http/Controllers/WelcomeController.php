<?php

namespace App\Http\Controllers;

use App\registration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{
    public function home(){
        return view('home');
    }

    public function all_users(){
        $userInfos =  registration::orderBy('id','desc')->get();
        return view('all_user',[
            'userInfos' => $userInfos
        ]);
    }

    public  function editUser($id) {
        //return $id;
        //return registration::find($id);
        $userById = registration::where('id',$id)->first();
        //return DB::table('registrations')->where('id',$id)->get();
        return view('edit-user-form',compact('userById'));
    }

    public  function updateUser(Request $request) {
        //return $id;
        $preUser = registration::find($request->id);
        $preUser->full_name = $request->name;
        $preUser->email = $request->email;
        $preUser->number = $request->number;
        $preUser->save();
        //$userById = registration::where('id',$id)->first();
        //return DB::table('registrations')->where('id',$id)->get();
        return redirect('/all_users')->with('success','information has been updated succesfully :) ');
    }

    public  function deleteUser($id) {
        //return $id;
        registration::find($id)->delete();
        return redirect('/all_users')->with('success','information has been delete succesfully :) ');
    }
}
