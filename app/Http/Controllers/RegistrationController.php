<?php

namespace App\Http\Controllers;

use App\registration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class RegistrationController extends Controller
{
    public function register(){
        return view('registration');
    }

    public function all_user()
    {
        $users=\App\registration::all();
        return view('all_user',compact('users'));
    }

    public function store(Request $request){
//        if($request->hasFile('filename')){
//            $file= $request->file('filename');
//            $name=time().$file->getClientOriginalName();
//            $file->move(public_path().'/images',$name);
//        }


        $data= new registration();

        $data->full_name= $request->get('name');
        $data->email= $request->get('email');
        $data->number= $request->get('number');
        $data->password= $request->get('password');
       // $date= $request->get('date');
        //$format =  $request->get('date');
        $data->date= $request->get('date');
        //$data->filename=$name;

        $data->save();

        return redirect('/all_users')->with('success','information has been added');

    }

}
